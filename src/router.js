import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import FirstHalf from './views/FirstHalf.vue';
import SecondHalf from './views/SecondHalf.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/first-half',
      name: 'firstHalf',
      component: FirstHalf,
    },
    {
      path: '/second-half',
      name: 'secondHalf',
      component: SecondHalf,
    },
  ],
});
