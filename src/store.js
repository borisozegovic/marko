import Vue from 'vue';
import Vuex from 'vuex';
import persons from './data/persons';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        persons,
        appVersion: 3
    },
    mutations: {},
    actions: {},
    getters: {
        appVersion: (state) => {
            return state.appVersion
        },

    }
});
